import * as postService from 'src/services/postService';
import { SET_POST } from './actionTypes';

const setPostAction = post => ({ type: SET_POST, payload: post });

export const loadPost = id => async dispatch => {
  const post = await postService.getPost(id);
  dispatch(setPostAction(post));
};

export const onUpdatePost = (id, body) => async (dispatch, getRootState) => {
  // eslint-disable-line
  const {
    updatedPost: { post }
  } = getRootState();
  if (post.body !== body) {
    await postService.updatePost(id, { body });
  }
  window.location.href = '/';
};
