import React, { Component } from 'react';
import { Form, Button, Segment } from 'semantic-ui-react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { loadPost, onUpdatePost } from './actions';
import styles from '../Thread/styles.module.scss';

class UpdatePost extends Component {
  state = { // eslint-disable-line
    isLoading: true,
    text: ''
  };

  componentDidMount() {
    const { profile } = this.props;
    if (profile.isAuthorized) {
      const { match } = this.props;
      this.props.loadPost(match.params.id); // eslint-disable-line
    }
  }

  componentDidUpdate(prev) {
    const { post } = this.props;
    if (!prev.post && post) {
      this.checkAuthor(post);
    }
  }

  onUpdateTwitt(e) {
    e.preventDefault();
    const { match: { params: { id } } } = this.props;
    const { text } = this.state;
    this.props.onUpdatePost(id, text); // eslint-disable-line
  }

  onChangeField(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  checkAuthor(post) {
    const { profile } = this.props;
    if (post.user.id !== profile.user.id) {
      window.location = '/';
    } else {
      this.setState({ isLoading: false, text: post.body });
    }
  }

  render() {
    const { isLoading, text } = this.state;
    return (
      <div className={styles.threadContent}>
        <h1>Update Twitt</h1>
        <Form name="loginForm" size="large" onSubmit={e => this.onUpdateTwitt(e)}>
          <Segment>
            <Form.Input
              fluid
              icon="at"
              iconPosition="left"
              placeholder="Text"
              type="text"
              name="text"
              value={text}
              onChange={e => this.onChangeField(e)}
            />
          </Segment>
          <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
            Save
          </Button>
        </Form>
      </div>
    );
  }
}

UpdatePost.propTypes = {
  match: PropTypes.objectOf(PropTypes.any).isRequired,
  post: PropTypes.any, // eslint-disable-line
  profile: PropTypes.objectOf(PropTypes.any).isRequired,
  loadPost: PropTypes.func.isRequired,
  onUpdatePost: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  profile: rootState.profile,
  post: rootState.updatedPost.post
});

export default connect(mapStateToProps, { loadPost, onUpdatePost })(UpdatePost);
