import { SET_POST } from './actionTypes';

const initState = {
  post: null
};

export default (state = initState, action) => {
  switch (action.type) {
    case SET_POST:
      return { ...state, post: action.payload };
    default:
      return state;
  }
};
