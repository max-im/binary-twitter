import * as commentService from 'src/services/commentService';
import { toggleExpandedPost } from '../Thread/actions';

export const editComment = (id, body) => async dispatch => {
  const postId = await commentService.editComment(id, { body });
  dispatch(toggleExpandedPost(postId));
};

export const onDeleteComment = id => async () => {
  await commentService.deleteComment(id);
  window.location.href = '/';
};
