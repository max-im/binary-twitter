import * as postService from 'src/services/postService';
import { DELETE_POST } from './actionTypes';

export const onDeletePost = postId => async dispatch => {
  const deletedId = await postService.deletePost(postId);
  dispatch({ type: DELETE_POST, payload: deletedId });
};
