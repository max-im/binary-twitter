/* eslint-disable */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button } from 'semantic-ui-react';

const EditComment = ({ body, saveComment }) => {
    const [commentText, setText] = useState(body);

    return (
        <Form reply onSubmit={() => saveComment(commentText)}>
            <Form.TextArea
                value={commentText}
                placeholder="Type a comment..."
                onChange={ev => setText(ev.target.value)}
            />
            <Button type="submit" content="Save comment" labelPosition="left" icon="save" primary />
        </Form>
    )
}

EditComment.propTypes = {
    body: PropTypes.string.isRequired,
    saveComment: PropTypes.func.isRequired
};

export default EditComment;
