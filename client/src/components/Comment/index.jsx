import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Icon, Label } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';
import EditComment from '../EditComment';

import styles from './styles.module.scss';

const Comment = ({ comment: { body, createdAt, user, id }, deleteComment, currentUserId, saveComment }) => {
  const [editMode, setEditMode] = useState(false);

  const onSaveComment = updatedComment => {
    saveComment(id, updatedComment);
    setEditMode(false);
  };

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {moment(createdAt).fromNow()}
        </CommentUI.Metadata>
        {currentUserId === user.id && (
          <>
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => setEditMode(true)}>
              <Icon name="edit" />
            </Label>
            <Label basic size="small" as="a" className={`${styles.toolbarBtn} red`} onClick={() => deleteComment(id)}>
              <Icon name="trash alternate" />
            </Label>
          </>
        )}
        {editMode && <EditComment id={id} body={body} saveComment={onSaveComment} />}
        {!editMode && (
          <CommentUI.Text>
            {body}
          </CommentUI.Text>
        )}
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  deleteComment: PropTypes.func.isRequired,
  currentUserId: PropTypes.string.isRequired,
  saveComment: PropTypes.func.isRequired
};

export default Comment;
