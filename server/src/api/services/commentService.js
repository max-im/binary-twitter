import commentRepository from '../../data/repositories/commentRepository';

export const create = (userId, comment) =>
  commentRepository.create({
    ...comment,
    userId
  });

export const getCommentById = id => commentRepository.getCommentById(id);

export const editComment = (id, body, userId) =>
  commentRepository.onEdit(id, body, userId);

export const deleteById = (id, userId) => {
  return commentRepository.onDelete(id, userId);
};
