import { Router } from 'express';
import * as commentService from '../services/commentService';

const router = Router();

router
  .get('/:id', (req, res, next) =>
    commentService
      .getCommentById(req.params.id)
      .then(comment => res.send(comment))
      .catch(next)
  )
  .post('/', (req, res, next) =>
    commentService
      .create(req.user.id, req.body)
      .then(comment => res.send(comment))
      .catch(next)
  );

router.put('/:id', (req, res, next) => {
  return commentService
    .editComment(req.params.id, req.body, req.user.id)
    .then(commentId => res.json(commentId))
    .catch(next);
});

router.delete('/:id', (req, res, next) => {
  return commentService
    .deleteById(req.params.id, req.user.id)
    .then(commentId => res.json(commentId))
    .catch(next);
});

export default router;
