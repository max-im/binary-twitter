import { CommentModel, UserModel, ImageModel } from '../models/index';
import BaseRepository from './baseRepository';

class CommentRepository extends BaseRepository {
  getCommentById(id) {
    return this.model.findOne({
      group: ['comment.id', 'user.id', 'user->image.id'],
      where: { id },
      include: [
        {
          model: UserModel,
          attributes: ['id', 'username'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        }
      ]
    });
  }

  async onEdit(id, body, userId) {
    const theComment = await this.getCommentById(id);
    if (theComment.user.id !== userId) {
      const err = new Error('Access denied');
      err.statusCode = 401;
      throw err;
    }
    return this.updateById(id, body).then(data => data.postId);
  }

  async onDelete(id, userId) {
    const theComment = await this.getCommentById(id);
    if (theComment.user.id !== userId) {
      const err = new Error('Access denied');
      err.statusCode = 401;
      throw err;
    }
    return this.deleteById(id).then(() => id);
  }
}

export default new CommentRepository(CommentModel);
